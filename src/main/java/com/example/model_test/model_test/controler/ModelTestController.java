package com.example.model_test.model_test.controler;

import com.example.model_test.model_test.dto.DtoModelTest;
import com.example.model_test.model_test.service.modeltest.IModelTestService;
import com.example.model_test.model_test.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/model_test")
public class ModelTestController {

    @Autowired
    private HttpServletRequest context;

    @Autowired
    private IModelTestService service;

    @PostMapping("/save")
    public ResponseEntity<Map<String, Object>> save(@RequestBody DtoModelTest dtoModelTest){
        return service.save(dtoModelTest);
    }
    @PostMapping("/save/participate/to/model_test/{model_test_id}")
    public ResponseEntity<Map<String, Object>> saveSingleParticipate(@PathVariable("model_test_id")  long id){
        return service.saveSingleParticipate(
                Long.parseLong(context.getAttribute(Constants.KEY_USER_ID).toString()), id);
    }

    @GetMapping("/all")
    public ResponseEntity<Map<String, Object>> getAll(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Map<String, Object>> getById(@PathVariable("id") long id){
        return service.getById(id);
    }
}
