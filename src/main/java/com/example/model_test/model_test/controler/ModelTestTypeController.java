package com.example.model_test.model_test.controler;

import com.example.model_test.model_test.model.ModelTestType;
import com.example.model_test.model_test.service.modeltest_type.IModelTestTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("api/v1/model_test/type")
public class ModelTestTypeController {

    @Autowired
    private IModelTestTypeService service;

    @PostMapping("/save")
    public ResponseEntity<Map<String ,Object>> save(@RequestBody ModelTestType type){
        return service.save(type);
    }

    @GetMapping("/all")
    public ResponseEntity<Map<String ,Object>> getType(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Map<String ,Object>> getType(@PathVariable("id") Integer id){
        return service.getById(id);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<Map<String ,Object>> delete(@PathVariable("id") Integer id){
        return service.delete(id);
    }

}
