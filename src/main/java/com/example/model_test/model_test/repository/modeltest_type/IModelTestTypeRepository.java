package com.example.model_test.model_test.repository.modeltest_type;

import com.example.model_test.model_test.model.ModelTestType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface IModelTestTypeRepository extends JpaRepository<ModelTestType, Integer> {
    @Query(value = "select t from model_test_types t where t.type like :type", nativeQuery = true)
    Optional<ModelTestType> findByType(@Param("type") String type);

}
