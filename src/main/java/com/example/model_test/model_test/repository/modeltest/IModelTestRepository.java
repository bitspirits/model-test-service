package com.example.model_test.model_test.repository.modeltest;

import com.example.model_test.model_test.model.ModelTest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IModelTestRepository extends JpaRepository<ModelTest, Long> {
}
