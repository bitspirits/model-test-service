package com.example.model_test.model_test.repository.modeltest_type;

import com.example.model_test.model_test.model.ModelTest;
import com.example.model_test.model_test.model.Participant;
import com.example.model_test.model_test.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IQuestionRepository extends JpaRepository<Question, Long> {
}
