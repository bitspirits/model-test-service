package com.example.model_test.model_test.repository.modeltest_type;

import com.example.model_test.model_test.model.ModelTest;
import com.example.model_test.model_test.model.Question;
import com.example.model_test.model_test.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ISubjectRepository extends JpaRepository<Subject, Long> {
    List<Subject> findByModelTest (ModelTest modelTest);
}
