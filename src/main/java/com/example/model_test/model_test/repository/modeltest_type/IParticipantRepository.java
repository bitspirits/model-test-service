package com.example.model_test.model_test.repository.modeltest_type;

import com.example.model_test.model_test.model.ModelTest;
import com.example.model_test.model_test.model.ModelTestType;
import com.example.model_test.model_test.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IParticipantRepository extends JpaRepository<Participant, Long> {
    List<Participant> findByModelTest (ModelTest modelTest);

//    @Query(value = "select p from participants p where p.model_test = :model_test_id and p.user_id = :user_id", nativeQuery = true)
//    List<Participant> findByUserIdAndModelTest(@Param("user_id") long id, @Param("model_test_id") long modelTestId);
}
