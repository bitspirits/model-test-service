package com.example.model_test.model_test.model;

import javax.persistence.*;

@Entity
@Table(name = "model_test_types")
public class ModelTestType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
