package com.example.model_test.model_test.model;

import javax.persistence.*;


@Entity
@Table(name = "participants")
public class Participant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;
    private long id;
    @ManyToOne
    private ModelTest modelTest;

    public Participant() {
    }

    public Participant(long userId, ModelTest modelTest) {
        this.userId = userId;
        this.modelTest = modelTest;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public ModelTest getModelTest() {
        return modelTest;
    }

    public void setModelTest(ModelTest modelTest) {
        this.modelTest = modelTest;
    }
}
