package com.example.model_test.model_test.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "test_subjects")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long subjectId;
    private long id;
    @ManyToOne
    private ModelTest modelTest;
    @OneToMany(targetEntity = Question.class, mappedBy = "subject")
    private List<Question> questions;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public ModelTest getModelTest() {
        return modelTest;
    }

    public void setModelTest(ModelTest modelTest) {
        this.modelTest = modelTest;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
