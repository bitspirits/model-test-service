package com.example.model_test.model_test.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "model_tests")
public class ModelTest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String title;
    @ManyToOne
    private ModelTestType type;
    // time in minutes
    private int time;
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date startTime;
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date endTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public ModelTestType getType() {
        return type;
    }

    public void setType(ModelTestType type) {
        this.type = type;
    }
}
