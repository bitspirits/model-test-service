package com.example.model_test.model_test.utils;

public class Constants {

    public static final String KEY_MESSAGE = "message";
    public static final String KEY_STATUS = "status";
    public static final String KEY_DATA = "data";
    public static final String KEY_TOKEN = "token";

    public static final String TOKEN_SECRET_KEY = "bcs2";
    public static final long TOKEN_VALIDATION = 1 * 365 * 24 * 60 * 60 * 1000;

    public static final String KEY_USER_ID = "id";
    public static final String KEY_USER_EMAIL = "email";
}
