package com.example.model_test.model_test.security;

import com.example.model_test.model_test.model.ApiResponsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class ApplicationUserDetailsService implements UserDetailsService {

    private String url = "http://localhost:8080/api/v1/users/my_info";

    @Autowired
    private RestTemplate template;

    @Autowired
    private WebClient.Builder client;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

       // HttpHeaders headers = new HttpHeaders();
// set `content-type` header
        //headers.setContentType(MediaType.APPLICATION_JSON);
        //headers.setBearerAuth(s);

//        Map<String, Object> map = new HashMap<>();
//        map.put("userId", 1);
//        map.put("title", "Spring Boot 101");
//        map.put("body", "A powerful tool for building web apps.");
        // build the request
//        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
//        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(headers);
         // HttpEntity request = new HttpEntity(headers);
// send POST request
//        ResponseModel response = template.get(
//                url,
//                HttpMethod.GET,
//                request,
//                ResponseModel.class,
//                1
//        );
//
//        if (response.getStatusCode() == HttpStatus.OK) {
//            response.getBody();
//        } else {
//        }

        //user.orElseThrow(()-> new UsernameNotFoundException("user with this email is not found"));

        ApiResponsModel response = client.build()
                .get()
                .uri(url)
                .header("Authorization", "Bearer "+s)
                .retrieve()
                .bodyToMono(ApiResponsModel.class)
                .block();

        return new ApplicationUserDetails(response.getData());

    }
}
