package com.example.model_test.model_test.security;

import com.example.model_test.model_test.authorization.AuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthFilter authFilter;

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
//        http.csrf().disable()
//                .authorizeRequests()
//                .antMatchers("/api/v1/users/register").permitAll()
//                .antMatchers("/api/v1/users/login").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/v1/**").hasAnyRole("USER","ADMIN")
//                .antMatchers(HttpMethod.POST,"/api/v1/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PUT,"/api/v1/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.DELETE,"/api/v1/**").hasRole("ADMIN")
//                .and()
//                .formLogin();

        // We don't need CSRF for this example
        httpSecurity.csrf().disable()
// dont authenticate this particular request
                .authorizeRequests()
                .antMatchers("/api/v1/users/register").permitAll()
                .antMatchers("/api/v1/users/login").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/v1/**").hasAnyRole("USER","ADMIN")
//                .antMatchers(HttpMethod.POST,"/api/v1/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PUT,"/api/v1/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.DELETE,"/api/v1/**").hasRole("ADMIN")
                .anyRequest()
                .authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        httpSecurity.addFilterBefore(authFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
