package com.example.model_test.model_test.service.modeltest_type;

import com.example.model_test.model_test.model.ModelTestType;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface IModelTestTypeService {
    ResponseEntity<Map<String ,Object>> save(ModelTestType type);
    ResponseEntity<Map<String ,Object>> getAll();
    ResponseEntity<Map<String ,Object>> getById(Integer id);
    ResponseEntity<Map<String ,Object>> delete(Integer id);
}
