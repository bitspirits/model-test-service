package com.example.model_test.model_test.service.modeltest;

import com.example.model_test.model_test.dto.DtoModelTest;
import com.example.model_test.model_test.model.ModelTest;
import com.example.model_test.model_test.model.Participant;
import com.example.model_test.model_test.model.Subject;
import com.example.model_test.model_test.repository.modeltest.IModelTestRepository;
import com.example.model_test.model_test.repository.modeltest_type.IParticipantRepository;
import com.example.model_test.model_test.repository.modeltest_type.IQuestionRepository;
import com.example.model_test.model_test.repository.modeltest_type.ISubjectRepository;
import com.example.model_test.model_test.utils.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ModelTestService implements IModelTestService{

    @Autowired
    private IModelTestRepository repository;

    @Autowired
    private IParticipantRepository participantRepository;

    @Autowired
    private IQuestionRepository questionRepository;

    @Autowired
    private ISubjectRepository subjectRepository;


    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ResponseEntity<Map<String, Object>> save(DtoModelTest dtoModelTest) {
        ModelTest modelTest = repository.save(dtoModelTest.getModelTest());
        if(modelTest!=null){
            dtoModelTest.getParticipants().forEach(participant -> {
                participant.setModelTest(modelTest);
            });
            dtoModelTest.getSubjects().forEach(question -> {
                question.setModelTest(modelTest);
            });
            participantRepository.saveAll(dtoModelTest.getParticipants());
            List<Subject> subjects = subjectRepository.saveAll(dtoModelTest.getSubjects());
            subjects.forEach(subject -> {
                subject.getQuestions().forEach(question -> {
                    question.setSubject(subject);
                });
                questionRepository.saveAll(subject.getQuestions());
            });

        }
        return new ResponseModel(HttpStatus.OK
                , "Model test save successfully"
                , true)
                .setData(modelTest)
                .build();
    }

    @Override
    public ResponseEntity<Map<String, Object>> saveSingleParticipate(Long userId, Long modelTestId) {
        TypedQuery<Participant> ps = entityManager.createQuery("select p from Participant p where p.modelTest = :model_test and p.userId = :user_id", Participant.class);
        List<Participant> participant = ps.setParameter("model_test", repository.findById(modelTestId).get()).setParameter("user_id", userId).getResultList();
         if(participant.size() == 0){
             return new ResponseModel(HttpStatus.OK
                     , "Single participate save successfully"
                     , true)
                     .setData(participantRepository.save(new Participant(userId, repository.findById(modelTestId).get())))
                     .build();
         }else {
             return new ResponseModel(HttpStatus.OK
                     , "Participant already exist"
                     , false)
                     .build();
         }
    }

    @Override
    public ResponseEntity<Map<String, Object>> getAll() {
        List<DtoModelTest> dtoModelTests = new ArrayList<>();
        List<ModelTest> modelTests = repository.findAll();

        modelTests.forEach(modelTest -> {
            List<Subject> subjects = subjectRepository.findByModelTest(modelTest);
            subjects.forEach(subject -> {
                subject.getQuestions().forEach(question -> {
                    question.setSubject(null);
                });
            });

            dtoModelTests.add(new DtoModelTest(
                    modelTest,
                    participantRepository.findByModelTest(modelTest),
                    subjects
            ));
        });
        return new ResponseModel(HttpStatus.OK
                , "All model test data"
                , true)
                .setData(dtoModelTests)
                .build();
    }

    @Override
    public ResponseEntity<Map<String, Object>> getById(long id) {
        ModelTest modelTest = repository.findById(id).get();
            List<Subject> subjects = subjectRepository.findByModelTest(modelTest);
            subjects.forEach(subject -> {
                subject.getQuestions().forEach(question -> {
                    question.setSubject(null);
                });
            });

            DtoModelTest  dtoModelTest = new DtoModelTest(
                    modelTest,
                    participantRepository.findByModelTest(modelTest),
                    subjects
            );

        return new ResponseModel(HttpStatus.OK
                , "All model test data"
                , true)
                .setData(dtoModelTest)
                .build();
    }
}
