package com.example.model_test.model_test.service.modeltest;

import com.example.model_test.model_test.dto.DtoModelTest;
import com.example.model_test.model_test.model.ModelTestType;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface IModelTestService {
    ResponseEntity<Map<String ,Object>> save(DtoModelTest modelTest);
    ResponseEntity<Map<String ,Object>> saveSingleParticipate(Long userId, Long modelTestId);
    ResponseEntity<Map<String ,Object>> getAll();
    ResponseEntity<Map<String ,Object>> getById(long id);
}
