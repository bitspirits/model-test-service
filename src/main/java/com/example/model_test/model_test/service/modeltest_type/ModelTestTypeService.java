package com.example.model_test.model_test.service.modeltest_type;

import com.example.model_test.model_test.model.ModelTestType;
import com.example.model_test.model_test.repository.modeltest_type.IModelTestTypeRepository;
import com.example.model_test.model_test.utils.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class ModelTestTypeService implements IModelTestTypeService {

    @Autowired
    private IModelTestTypeRepository repository;

    @Override
    public ResponseEntity<Map<String, Object>> save(ModelTestType type) {
        if (!repository.findByType(type.getType()).isPresent()) {
            return new ResponseModel(HttpStatus.OK
                    , type.getId() == 0 ? " Model test type save successfully" : "Model test type update successfully"
                    , true)
                    .setData(repository.save(type))
                    .build();
        } else {
            return new ResponseModel(HttpStatus.OK
                    , "Type Already Exist"
                    , false)
                    .build();
        }
    }

    @Override
    public ResponseEntity<Map<String, Object>> getAll() {
        return new ResponseModel(HttpStatus.OK
                , "All type fetch successfully"
                , true)
                .setData(repository.findAll())
                .build();
    }

    @Override
    public ResponseEntity<Map<String, Object>> getById(Integer id) {
        Optional<ModelTestType> type = repository.findById(id);

        return new ResponseModel(HttpStatus.OK
                , type.get() != null ? "Type fetch successfully" : "No type found with this id"
                , type.get() != null ? true : false)
                .setData(type.get())
                .build();
    }

    @Override
    public ResponseEntity<Map<String, Object>> delete(Integer id) {
        Optional<ModelTestType> type = repository.findById(id);
        if (type.get() != null) repository.delete(type.get());
        return new ResponseModel(HttpStatus.OK
                , type.get() != null ? "Type delete successfully" : "No type found with this id"
                , type.get() != null ? true : false)
                .build();
    }
}
