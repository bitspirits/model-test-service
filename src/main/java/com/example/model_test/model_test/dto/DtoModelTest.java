package com.example.model_test.model_test.dto;

import com.example.model_test.model_test.model.ModelTest;
import com.example.model_test.model_test.model.Participant;
import com.example.model_test.model_test.model.Question;
import com.example.model_test.model_test.model.Subject;

import java.util.List;

public class DtoModelTest {
    private ModelTest modelTest;
    private List<Participant> participants;
    private List<Subject> subjects;

    public DtoModelTest(ModelTest modelTest, List<Participant> participants, List<Subject> subjects) {
        this.modelTest = modelTest;
        this.participants = participants;
        this.subjects = subjects;
        this.participants.forEach(participant -> {
            participant.setModelTest(null);
        });
        this.subjects.forEach(question -> {
            question.setModelTest(null);
        });
    }

    public ModelTest getModelTest() {
        return modelTest;
    }

    public void setModelTest(ModelTest modelTest) {
        this.modelTest = modelTest;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
