package com.example.model_test.model_test.authorization;

import com.example.model_test.model_test.model.User;
import com.example.model_test.model_test.utils.Constants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class JWTGenerator {

    public static String getToken(User user){
        long times = System.currentTimeMillis();
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS256, Constants.TOKEN_SECRET_KEY)
                .setIssuedAt(new Date(times))
                .setExpiration(new Date(times + Constants.TOKEN_VALIDATION))
                .claim(Constants.KEY_USER_ID, user.getId())
                .claim(Constants.KEY_USER_EMAIL, user.getEmail())
                .compact();

    }

    public static Claims getClaimsByToken(String token){
        return  Jwts.parser().setSigningKey(Constants.TOKEN_SECRET_KEY).parseClaimsJws(token).getBody();
    }
}
