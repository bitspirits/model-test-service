package com.example.model_test.model_test.authorization;

import com.example.model_test.model_test.security.ApplicationUserDetailsService;
import com.example.model_test.model_test.utils.Constants;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthFilter extends OncePerRequestFilter {

    @Autowired
    private ApplicationUserDetailsService service;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        HttpServletResponse response =  httpServletResponse;
        HttpServletRequest request =  httpServletRequest;

        String header = request.getHeader("Authorization");

        String path = request.getRequestURI();
        if ("/api/v1/users/login".equals(path) || "/api/v1/users/register".equals(path)) {
            filterChain.doFilter(request, response);
            return;
        }

        if (header != null) {
            String[] headerArr = header.split("Bearer");
            if (headerArr.length > 1 && headerArr[1] != null) {

                try {
                    String token = headerArr[1];
                    Claims claims = JWTGenerator.getClaimsByToken(token);
                    request.setAttribute(Constants.KEY_USER_ID, claims.get(Constants.KEY_USER_ID));
                    request.setAttribute(Constants.KEY_USER_EMAIL, claims.get(Constants.KEY_USER_EMAIL));

                    UserDetails userDetails = service.loadUserByUsername(token);
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                            new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                } catch (Exception e) {
                    response.sendError(HttpStatus.FORBIDDEN.value(), "Please provide valid token");
                    return;
                }


            } else {
                response.sendError(HttpStatus.FORBIDDEN.value(), "Bearer must be provider or token not be null");
                return;
            }
        } else {
            response.sendError(HttpStatus.FORBIDDEN.value(), "Authorization token must be provided");
            return;
        }

        filterChain.doFilter(request, response);


    }
}
